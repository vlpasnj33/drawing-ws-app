module drawing-ws-app

go 1.21.2

require (
	github.com/lucasb-eyer/go-colorful v1.2.0
	github.com/tidwall/gjson v1.17.0
)

require golang.org/x/net v0.17.0 // indirect

require (
	github.com/google/uuid v1.5.0
	github.com/gorilla/websocket v1.5.1
	github.com/tidwall/match v1.1.1 // indirect
	github.com/tidwall/pretty v1.2.0 // indirect
)
