package utils

import (
	"math/rand"
	"time"
	color "github.com/lucasb-eyer/go-colorful"
)

func Init() {
	seed := time.Now().UnixNano()
	rand.New(rand.NewSource(seed))
}

func GenerateColor() string {
	c := color.Hsv(rand.Float64() * 360.0, 0.8, 0.8)
	return c.Hex()
}

