package messages

const (
	KindConnected = iota + 1
	KindUserJoined
	KindUserLeft
	KindStroke
	KindClear
)

type Point struct {
	X int `json:"x"`
	Y int `json:"y"`
}
  
type User struct {
	ID    string `json:"id"`
	Color string `json:"color"`
}

type Connected struct {
	Kind  int    `json:"kind"`
	Color string `json:"color"`
	Users []User `json:"users"`
}

func NewConnected(color string, users []User) *Connected {
	return &Connected{
	  	Kind:  KindConnected,
	  	Color: color,
	  	Users: users,
	}
}

type UserJoined struct {
	Kind int  `json:"kind"`
	User User `json:"user"`
 }
  
func NewUserJoined(userID string, color string) *UserJoined {
	return &UserJoined{
		Kind: KindUserJoined,
	  	User: User{ID: userID, Color: color},
	}
}

type UserLeft struct {
	Kind   int    `json:"kind"`
	UserID string `json:"userId"`
}
  
func NewUserLeft(userID string) *UserLeft {
	return &UserLeft{
		Kind:   KindUserLeft,
	  	UserID: userID,
	}
}

type Stroke struct {
	Kind   int     `json:"kind"`
	UserID string  `json:"userId"`
	Points []Point `json:"points"`
	Finish bool    `json:"finish"`
}
  
type Clear struct {
	Kind   int    `json:"kind"`
	UserID string `json:"userId"`
}