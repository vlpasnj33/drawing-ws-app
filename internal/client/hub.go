package client

import (
	"drawing-ws-app/internal/messages"
	"encoding/json"
	"log"
	"net/http"

	"github.com/gorilla/websocket"
	"github.com/tidwall/gjson"

)

var upgrader = websocket.Upgrader{
	CheckOrigin: func(r *http.Request) bool { return true },
}

type Hub struct {
	clients    []*Client
	register   chan *Client
	unregister chan *Client
}

func NewHub() *Hub {
	return &Hub{
		clients:    make([]*Client, 0),
		register:   make(chan *Client),
		unregister: make(chan *Client),
	}
}

func (h *Hub) Run() {
	for {
		select {
		case client := <-h.register:
			h.onConnect(client)
		case client := <-h.unregister:
			h.onDisconnect(client)
		}
	}
}

func (h *Hub) HandleWebSocket(w http.ResponseWriter, r *http.Request) {
	socket, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
	  	http.Error(w, "could not upgrade", http.StatusInternalServerError)
	  	return
	}
	client := NewClient(h, socket)
	h.clients = append(h.clients, client)
	h.register <- client
	client.run()
}

func (h *Hub) send(message interface{}, client *Client) {
	data, _ := json.Marshal(message)
	client.Outbound <- data
}

func (h *Hub) broadcast(message interface{}, ignore *Client) {
	data, _ := json.Marshal(message)
	for _, c := range h.clients {
		if c != ignore {
			c.Outbound <- data
	  	}
	}
}

func (h *Hub) onConnect(c *Client) {
	log.Println("client connected: ", c.Socket.RemoteAddr())

	users := []messages.User{}
	for _, c := range h.clients {
		users = append(users, messages.User{ID: c.Id, Color: c.Color})
	}

	h.send(messages.NewConnected(c.Color, users), c)
	h.broadcast(messages.NewUserJoined(c.Id, c.Color), c)
}

func (h *Hub) onDisconnect(c *Client) {
	log.Println("client disconnected: ", c.Socket.RemoteAddr())
	c.close()

	i := -1
	for j, v := range h.clients {
		if v.Id == c.Id {
			i = j
			break
		}
	}

	copy(h.clients[i:], h.clients[i+1:])
	h.clients[len(h.clients) - 1] = nil 
	h.clients = h.clients[:len(h.clients) - 1]

	h.broadcast(messages.NewUserLeft(c.Id), nil)
}

func (h *Hub) onMessage(data []byte, c *Client) {
	kind := gjson.GetBytes(data, "kind").Int()
	if kind == messages.KindStroke {
		var msg messages.Stroke
		if json.Unmarshal(data, &msg) != nil {
		  	return
		}
		msg.UserID = c.Id
    	h.broadcast(msg, c)
	} else if kind == messages.KindClear {
		var msg messages.Clear
		if json.Unmarshal(data, &msg) != nil {
		  return
		}
		msg.UserID = c.Id
		h.broadcast(msg, c)
	}
}