package client

import (
	"drawing-ws-app/internal/utils"

	"github.com/google/uuid"
	"github.com/gorilla/websocket"
)

type Client struct {
	Id     		string
	Hub    		*Hub
	Color  		string
	Socket 		*websocket.Conn
	Outbound	chan []byte
}

func NewClient(hub *Hub, socket *websocket.Conn) *Client {
	return &Client{
		Id: 		uuid.NewString(),
		Hub: 		hub,	
		Color:		utils.GenerateColor(),
		Socket: 	socket,
		Outbound: 	make(chan []byte), 
	}
}

func (c *Client) Read() {
	defer func() {
		c.Hub.unregister <- c
	}()

	for {
		_, data, err := c.Socket.ReadMessage()
		if err != nil {
			break
		}
		c.Hub.onMessage(data, c)
	}
}

func (c *Client) Write() {
	for {
	  select {
	  case data, ok := <-c.Outbound:
		if !ok {
		  c.Socket.WriteMessage(websocket.CloseMessage, []byte{})
		  return
		}
		c.Socket.WriteMessage(websocket.TextMessage, data)
	  }
	}
}

func (c Client) run() {
	go c.Read()
	go c.Write()
}
  
func (c Client) close() {
	c.Socket.Close()
	close(c.Outbound)
}