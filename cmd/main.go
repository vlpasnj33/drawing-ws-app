package main

import (
	"drawing-ws-app/internal/client"
	"log"
	"net/http"
)

func main() {
	hub := client.NewHub()

	go hub.Run()

	http.HandleFunc("/ws", hub.HandleWebSocket)

	err := http.ListenAndServe(":3000", nil)
	if err != nil {
		log.Fatal(err)
	}
}

